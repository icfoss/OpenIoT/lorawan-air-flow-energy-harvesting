### LoRaWAN Air Flow Energy Harvesting - Air Quality Node
Harvesting energy plays an important role in increasing the efficiency and lifetime of IoT devices. Energy harvesting systems have some limitations such as unavailability of the energy source from which energy is supposed to be harvested, low amount of harvested energy, inefficiency of the harvesting system, etc. To overcome these limitations, some efforts have been done and new models for harvesting energy have been formed which are discussed in this review concerning the energy source of the harvest. A miniature DC motor is used to harvest energy from Air Flow. The harvested energy is stored into a Li-ion battery which is able to supply required amount of power to LoRaWAN Air Quality node based on BME680.

### Prerequisites
1. LoRaWAN & STM32 based Controller: https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0
2. Miniature DC motor: https://datasheetspdf.com/datasheet/RF-300FA-12350.html
3. LM2596 HV DC-DC Buck Converter: https://robu.in/product/lm2596-hv-dc-dc-buck-converter-4-5-50v-to-3-35v/
4. Energy Harvester Generic Board for LPWAN: https://gitlab.com/soorajvs13/energy-harvester-generic-board-for-lpwan
5. 14mAh Battery: https://www.mouser.in/ProductDetail/Nichicon/SLB08115L1401PM?qs=DRkmTr78QARoHNFnJd5OYQ%3D%3D
	OR
150mAh Battery: https://www.mouser.in/ProductDetail/Nichicon/SLB12400L1511CA?qs=DRkmTr78QARr5WxZbC3vqA%3D%3D	
6. BME680: https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680/
7. Source Code: https://gitlab.com/soorajvs13/bme680-stm32-integration
8. Project Link: https://www.electronicsforu.com/electronics-projects/hardware-diy/5v-portable-wind-turbine

